require "sqlite3"
class Post
  @@SQLITE_DB_FILE = 'notepad.sqlite'
  def self.post_types
    {'Memo' => Memo, 'Task' => Task, 'Link' => Link}
  end

  def self.create(type)
    return post_types[type].new
  end

  def initialize
    @created_at = Time.now
    @text = nil
  end
  def read_from_console
  end

  def to_strings
  end

  def to_db_hash
    {
      'type' => self.class.name,
      'created_at' => @created_at.to_s
    }
  end

  def save_to_db
    db= SQLite3::Database.open(@@SQLite_DB_FILE)
    db.results_as_hash = true

    db.execute("INSERT INTO posts (" + to_db_hash.keys.join(', ') +")" +
               "VALUES ( " +
                       ('?,'*to_db_hash.keys.size).chomp(',') +
                       ")",
               to_db_hash.values
               )
    insert_row_id = db.last_insert_row_id
    db.close
    return insert_row_id
  end

  def load_data(data_hash)
    @created_at = Time.parse(data_hash['created_at'])
  end

  def save
  end
  def self.find(limit, type, id)
    db = SQLite3::Database.open(@@SQLITE_DB_FILE)
    if id.present?
      db.results_as_hash = true
      result = db.execute("SELECT * FROM posts WHERE rowid=?", id)
      result = result[0] if result.is_a? Array
      db.close
      if result.empty?
        puts "This id:#{id} not found in the database"
        return nil
      else
        post = create(result['type'])
        post.load_data(result)
        return post
      end

      else
      db.results_as_hash = false
      query = "SELECT * FROM posts"
      query += "where type = :type" unless type.nil?
      query += "order by rowid DESC"
      query += "LIMIT :limit" unless limit.nil?

      statment = db.prepare query

      statment.bind_param('type', type) unless type.nil?
      statment.bind_param('limit', limit) unless limit.nil?

      result = statment.execute!
      statment.close
      db.close


    end

  end

end
